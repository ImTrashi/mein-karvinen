const process = require('process');
var args = process.argv.slice(2);

console.log("Total number of arguments are: "+args.length);
args.forEach((val, index) => {
    console.log(`${index}: ${val}`);
});

const luku1 = args[0];
const operaattori = args[1];
const luku2 = args[2];

if (operaattori == "-") {
    const tulos = luku1 - luku2;
    console.log(`${luku1} "-" ${luku2} = ${tulos}`);
    console.log(luku1 + " - " + luku2 + " = " + tulos);
}

let tulos = "invalid";
switch (operaattori) {
    case "+":
        tulos = parseFloat(luku1) + parseFloat(luku2);
        break;
    case "/":
        tulos = parseFloat(luku1) / parseFloat(luku2);
        break;
    case "*":
        tulos = parseFloat(luku1) * parseFloat(luku2);
        break;
    case "%":
        tulos = parseFloat(luku1) % parseFloat(luku2);
        break;
    default:
        break;
}
console.log(`${luku1} ${operaattori} ${luku2} = ${tulos}`);